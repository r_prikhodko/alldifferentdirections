<?php

use AllDifferentDirections\App;

require_once('../autoloader.php');

$app = new App();
$app->start(file_get_contents('input'));