<?php

/**
 * PSR-4 Loader
 * @see https://www.php-fig.org/psr/psr-4/
 */
spl_autoload_register(function($class) {
    $project_prefix = 'AllDifferentDirections\\';  // Lingua default namespace
    $project_dir = __DIR__ . '/src/';    // Include directory path
    $lib_dir = __DIR__ . '/libs/';    // Is used when the prefix is not equal to internal 'Lingua' prefix

    if (strncmp($project_prefix, $class, strlen($project_prefix)) === 0) {  // For internal project files (with Lingua namespace)
        $relative_class = substr($class, strlen($project_prefix));
        $file = $project_dir . str_replace('\\', '/', $relative_class) . '.php';
        if (file_exists($file)) {
            require $file;
        } else return;
    } else {
        $file = $lib_dir . str_replace('\\', '/', $class) . '.php'; // For external (libs) files
        if (file_exists($file)) {
            require $file;
        } else return;
    }
});