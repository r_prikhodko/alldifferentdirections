<?php

namespace AllDifferentDirections;

use AllDifferentDirections\DestinationFinder\Finder;

class App {
    /**
     * Starts the application :)
     * @param $input
     * @throws \InvalidArgumentException
     */
    public function start(string $input) {
        $rawCommandsFragments = $this->divideInput($input);
        foreach ($rawCommandsFragments as $fragment) {
            $finder = new Finder();
            $finder->processRawCommandsFragment($fragment);
            $avgPosition = $finder->getAveragePosition();
            $lengthToWorstPosition = $finder->getDistanceToWorstPosition($avgPosition);

            echo sprintf("%.2f %.2f %.2f ", $avgPosition->x, $avgPosition->y, $lengthToWorstPosition);
        }
    }

    /**
     * Devides the input into blocks (fragments)
     * @param string $input
     * @throws \InvalidArgumentException
     * @return array
     */
    protected function divideInput(string $input) : array {
        $inputArray = explode(PHP_EOL, trim($input, '\n'));
        if (!$inputArray) {
            throw new \InvalidArgumentException('Invald input. Input is empty');
        } elseif (!is_numeric($inputArray[0])) {
            throw new \InvalidArgumentException('Invald input. First line of the file must be int number (number of people met)');
        } 

        $fragments = [];
        while($inputArray) {
            $rawCommandsNumber = array_shift($inputArray);
            $rawCommandsFragment = array_splice($inputArray, 0, (int)$rawCommandsNumber);
            
            if ($rawCommandsNumber == 0) {
                break;
            } array_push($fragments, $rawCommandsFragment);
        }
        
        return $fragments;
    }
}