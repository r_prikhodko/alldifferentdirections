<?php

namespace AllDifferentDirections\DestinationFinder;

class Position {
    /** @var float */
    public $x;

    /** @var float */
    public $y;

    public function __construct(float $x = 0.0, float $y = 0.0) {
        $this->x = $x;
        $this->y = $y;
    }
}