<?php

namespace AllDifferentDirections\DestinationFinder;

use AllDifferentDirections\DestinationFinder\Position;
use AllDifferentDirections\DestinationFinder\Command;

class Finder {
    /** @var array */
    protected $points = [];

    /**
     * Processes raw commands block (fragment)
     * @param array $rawCommandsFragment
     * @throws \InvalidArgumentException
     */
    public function processRawCommandsFragment(array $rawCommandsFragmet) {
        if (!$rawCommandsFragmet) {
            throw new \InvalidArgumentException('Raw commands fragment must not be empty');
        }

        foreach ($rawCommandsFragmet as $rawCommand) {
            $command = new Command();
            $command->parseRawCommand($rawCommand);
            array_push($this->points, $command->getPosition());
        }
    }

    /**
     * Returns average position (fragment)
     * @return Position 
     */
    public function getAveragePosition() : Position {
        $pointsSummary = new Position(0, 0);
        $numberOfPoints = count($this->points);
        foreach ($this->points as $point) {
            $pointsSummary->x += $point->x;
            $pointsSummary->y += $point->y;
        }

        return new Position(
            $pointsSummary->x = $pointsSummary->x / $numberOfPoints, 
            $pointsSummary->y = $pointsSummary->y / $numberOfPoints
        );
    }

    /**
     * Returns the distance between the best and the worst position
     * @param Position $bestPosition
     * @return float
     */
    public function getDistanceToWorstPosition(Position $bestPosition) : float {
        $worstDistance = 0;
        foreach ($this->points as $point) {
            $distance = $this->distanceBetweenPositions($bestPosition, $point);
            $worstDistance = $distance > $worstDistance 
                         ? $distance 
                         : $worstDistance;
        }

        return $worstDistance;
    }

    /**
     * Finds the distance (length) between two points
     * @param Position $a
     * @param Position $b
     * @return float
     */
    protected function distanceBetweenPositions(Position $a, Position $b) : float {
        return sqrt((($a->x - $b->x) ** 2) + (($a->y - $b->y) ** 2));
    }
}