<?php

namespace AllDifferentDirections\DestinationFinder;

use AllDifferentDirections\DestinationFinder\Position;

class Command {
    /** @var Position */
    protected $position;

    /** @var float */
    protected $angle;

    /**
     * Sets start position
     * @param Position
     * @return self
     */
    public function setStartPosition(Position $pos) : self {
        $this->position = $pos;
        return $this;
    }

    /**
     * Sets start angle
     * @param float @angle
     * @return self
     */
    public function start(float $angle) : self {
        $this->angle = $angle;
        return $this;
    }
    
    /**
     * Makes `walk` action
     * @param int $steps
     * @return self
     */
    public function walk(int $steps) : self {
        $this->position->x += $steps * cos(deg2rad($this->angle));
        $this->position->y += $steps * sin(deg2rad($this->angle));
        return $this;
    }

    /**
     * Makes `turn` action
     * @param float angle
     * @return self
     */
    public function turn(float $angle) {
        $this->angle += $angle;
        return $this;
    }

    /**
     * Initializes itself by raw command
     * @param string $rawCommand
     * @throws \InvalidArgumentException
     */
    public function parseRawCommand(string $rawCommand) {
        // Raw command example: "2.6762 75.2811 start -45.0 walk 40 turn 40.0 walk 60"
        $commandArray = explode(' ', $rawCommand);

        if (count($commandArray) < 4) {
            throw new \InvalidArgumentException("Invalid command. Each command must contain at least x, y positions and `start` action");
        } elseif (
            !is_numeric($commandArray[0]) ||
            !is_numeric($commandArray[1])
        ) {
            throw new \InvalidArgumentException("Invalid command. First two arguments must be x, y positions");
        } elseif ($commandArray[2] !== 'start') {
            throw new \InvalidArgumentException('Invalid command. Each command must contain `start` action, it must be placed first');
        }

        $this->setStartPosition(
            new Position(array_shift($commandArray), array_shift($commandArray))
        );
        
        while($commandArray) {
            $action = array_shift($commandArray);
            $value = array_shift($commandArray);

            if (!is_numeric($value)) {
                throw new \InvalidArgumentException("Value for `$action` should be numeric");
            } else if (
                $action !== 'start' &&
                $action !== 'walk' &&
                $action !== 'turn'
            ) {
                throw new \InvalidArgumentException("Invalid action: `$action`");
            }

            $this->{$action}((float)$value);
        }
    }

    /** 
     * Returns position
     * @return Position
     */
    public function getPosition() : Position {
        return $this->position;
    }
}